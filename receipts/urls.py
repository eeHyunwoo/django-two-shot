from django.urls import path
from receipts.views import create_account, create_category, show_receipts_list, create_receipt, show_expenses_list, show_accounts_list

urlpatterns = [
    path("", show_receipts_list, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", show_expenses_list, name="category_list"),
    path("accounts/", show_accounts_list, name="account_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]
